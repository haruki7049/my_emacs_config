;; FONT SETTING
(set-face-attribute 'default nil
  :family "UDEV Gothic NF"
  :height 120)

;; Backup files
(setq make-backup-files nil)
(setq auto-save-default nil)

;; Delete welcome message
(setq inhibit-startup-message t)

;; Add News Feed to newsticker.el
(setq newsticker-url-list
      '(("deno" "https://deno.com/feed")
	("this week in rust" "https://this-week-in-rust.org/rss.xml")
	("Rust-lang Main blog" "https://blog.rust-lang.org/feed.xml")
	("Rust-lang 'Inside rust' blog" "https://blog.rust-lang.org/inside-rust/feed.xml")
	("zenn.dev - webrtc" "https://zenn.dev/topics/webrtc/feed")
	("zenn.dev - Rust" "https://zenn.dev/topics/rust/feed")
	("zenn.dev - FreeBSD" "https://zenn.dev/topics/freebsd/feed")
	("zenn.dev - TypeScript" "https://zenn.dev/topics/typescript/feed")
	("zenn.dev - Deno" "https://zenn.dev/topics/deno/feed")
	("zenn.dev - React" "https://zenn.dev/topics/react/feed")))

;; LEAF setup
(prog1 "prepare leaf"
  (prog1 "package"
    (custom-set-variables
     '(package-archives '(("org"   . "https://orgmode.org/elpa/")
                          ("melpa" . "https://melpa.org/packages/")
                          ("gnu"   . "https://elpa.gnu.org/packages/"))))
    (package-initialize))

  (prog1 "leaf"
    (unless (package-installed-p 'leaf)
      (unless (assoc 'leaf package-archive-contents)
        (package-refresh-contents))
      (condition-case err
          (package-install 'leaf)
        (error
         (package-refresh-contents)       ; renew local melpa cache if fail
         (package-install 'leaf))))

    (leaf leaf-keywords
      :ensure t
      :config (leaf-keywords-init)))

  (prog1 "optional packages for leaf-keywords"
    ;; optional packages if you want to use :hydra, :el-get,,,
    (leaf hydra :ensure t)
    (leaf el-get :ensure t
      :custom ((el-get-git-shallow-clone  . t)))))

;; Package settings by LEAF
(leaf *lsp-tools
  :config
  (leaf lsp-mode
    :ensure t
    :config
    (leaf ruby-mode
      :ensure t)
    (leaf rust-mode
      :ensure t
      :config
      (leaf flycheck
        :ensure t))
      (leaf lsp-ui
        :ensure t)))

(leaf *theme
  :config
  (leaf iceberg-theme
    :ensure t
    :config
    (iceberg-theme-create-theme-file)
    (load-theme 'solarized-iceberg-dark t)))

(leaf *other-tools
  :config
  (leaf gnus
    :ensure t)
  (leaf ement
    :ensure t)
  (leaf mastodon
    :ensure t
    :custom ((mastodon-instance-url . "https://misskey.io")
	     (mastodon-active-user . "haruki7049"))))

(leaf *git-tools
  :config
  (leaf magit
    :ensure t))

(leaf leaf
  :config
  (leaf leaf-convert
    :ensure t)
  (leaf leaf-tree
    :ensure t
    :custom ((imenu-list-size . 30)
      (imenu-list-position . 'left))))

(leaf macrostep
  :ensure t
  :bind (("C-c e" . macrostep-expand)))
